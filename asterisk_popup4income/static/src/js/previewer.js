// NOTE: Rename to popuper or something like this... 

odoo.define('asterisk_popup4income.previewer', function (require) {
    "use strict";

    var HEADER_TEMPLATE_DEFAULT = '<span>{{asterisk.caller_number}}</span>';

    var webClient = require('web.web_client');
    var BaseWidget = require('web.Widget');
    var data_manager = require('web.data_manager');
    var pyeval = require('web.pyeval');
    var formaters = require('asterisk_popup4income.formaters');

    var Previewer = BaseWidget.extend({
        template: 'asterisk_popup4income.Previwer',

        init: function(context, settings, headerTempalate) {
            this._super(null);
            this.__header__ = headerTempalate
            this.context = context;
            this.settings = settings;
            this.selection = {};
        },

        events: {
            'click .asterisk_popup4income__previwer-item-record.clickable': 'onRecordItemClick',
            'click .asterisk_popup4income__previwer__action': 'onRecordActionClick',
            'click .asterisk_popup4income__previwer__impersonal-action': 'onImpersonalActionClick',
            'click .close': 'close'
        },

        open: function() {
            var self = this;

            self.appendTo(document.body).then(function() {
                // else animate will ignore by browsers
                self.$el.focus().addClass("asterisk_popup4income__previwer--visible");
            });
        },

        close: function () {
            this.$el.removeClass("asterisk_popup4income__previwer--visible");
            // see css sty;e for delay
            setTimeout(function() {
                this.destroy();
            }.bind(this), 400);
        },
        
        onRecordItemClick: function(event) {
            event.stopPropagation();
            event.preventDefault();

            var target = $(event.currentTarget);

            // reset active state
            target.parent(".asterisk_popup4income__previwer__list-group")
                  .find(".asterisk_popup4income__previwer-item-record")
                  .removeClass("asterisk_popup4income__previwer-item-record--active")
                  .removeClass("list-group-item-danger")
                  .addClass("list-group-item-success");

            // add active state
            target.removeClass("list-group-item-success")
                  .addClass("list-group-item-danger")
                  .addClass("asterisk_popup4income__previwer-item-record--active");

            var setting = this.findArrayElement(this.settings, target.attr('data-setting-id'), "settings");
            var record = this.findArrayElement(setting.records, target.attr('data-record-id'), "records");

            this.selection = {setting: setting, record: record};
        },

        onRecordActionClick: function (event) {
            event.stopPropagation();
            event.preventDefault();

            if (_.isEmpty(this.selection)) {
                console.log("has no selection");
                return;
            }

            var actionDefinitionIndex = Number($(event.currentTarget).attr('data-action-json-definition-index'));
            var actionDefinition = JSON.parse(JSON.stringify(this.selection.setting.__record_actions__[actionDefinitionIndex]));

            if (!_.isEmpty(actionDefinition)) {
                var actionId = actionDefinition["action"];
                var actionOptions = actionDefinition["action-args"]
                var actionDomain = actionDefinition["action-domain"];
                var actionContext = actionDefinition["action-context"];

                this.doAction(
                    actionId,
                    actionOptions,
                    actionDomain,
                    actionContext,
                    this.selection.setting,
                    this.selection.record
                );
            }
        },

        onImpersonalActionClick: function(event) {
            event.stopPropagation();
            event.preventDefault();

            var target = $(event.currentTarget);

            var setting = this.findArrayElement(this.settings, target.attr('data-action-setting-id'), "settings");
            var actionDefinitionIndex = target.attr('data-action-json-definition-index');
            var actionDefinition = JSON.parse(JSON.stringify(setting.__impersonal_actions__[actionDefinitionIndex]));

            if (!_.isEmpty(actionDefinition)) {
                var actionId = actionDefinition["action"];
                var actionOptions = actionDefinition["action-args"]
                var actionDomain = actionDefinition["action-domain"];
                var actionContext = actionDefinition["action-context"];

                this.doAction(
                    actionId,
                    actionOptions,
                    actionDomain,
                    actionContext,
                    setting
                );
            }
        },

        doAction: function(actionId, actionOptions, actionDomain, actionContext, configuration, record) {
            var forwardingMode, self = this;

            _.defaults(actionOptions, {
                clear_breadcrumbs: true
            });
            
            if (actionOptions.view_type == 'form') {
                forwardingMode = "form.new";
                // computed correct res_id

                if (!_.isEmpty(actionOptions['res_id'])) {
                    forwardingMode = "form.edit";

                    if (actionOptions['res_id'] === '.') {
                        actionOptions['res_id'] = (record || {}).id;
                    }
                }
            } else {
                // list by now have one mode
                // but maybe we need also list.edit?
                forwardingMode = "list.new";
            }

            // load action definition
            return data_manager.load_action(actionId, {}).then(function(actionDefinition) {
                
                actionDefinition = JSON.parse(JSON.stringify(actionDefinition));

                if (!_.isEmpty(configuration.fieldsForwarding)) {

                    // forwarding mode must be:
                    // `form.new` and/or `form.edit` and/or `list.edit`
                    // and set in forwarding rules as property `mode`
                    var thisModelForwarding = self.doFilterForwarding(
                        configuration.fieldsForwarding[actionDefinition.res_model],
                        forwardingMode
                    );

                    actionOptions.additional_context = {
                        goToEditMode: true,
                        popup4income_autocomplete: {
                            // forwarding: configuration.fieldsForwarding[actionDefinition.res_model],
                            forwarding: thisModelForwarding,
                            record: record,
                            asterisk: configuration.externalContext
                        }
                    };
                }

                // replace action context if need
                if (!_.isEmpty(actionContext) && !_.isEmpty(actionDefinition.context)) {
                    actionDefinition.context = _.defaults(actionContext, actionDefinition.context);
                    console.log("action context was modify", actionDefinition.context);
                }

                // NOTE - replace with `asterisk_popup4income.formaters.domain`
                // override action domain with own domain from button attribute
                if (!_.isEmpty(actionDomain) && _.isArray(actionDomain)) {

                    // domain values interpolation with contexts
                    // actionDomain common kind - [['leftOperand', 'operator', '{placeholder.property}'], ...]

                    actionDomain = formaters.domain(actionDomain, {
                        record: record,
                        asterisk: configuration.externalContext
                    });

                    actionDefinition.domain = actionDomain;
                    console.log("action domain was modify", actionDefinition.domain);
                }

                webClient.action_manager.do_action(actionDefinition, actionOptions).then(function() {
                    webClient.menu.open_action(actionDefinition.id);
                });
            });
        },
        
        findArrayElement: function (array, id, arrayName) {
            var element = _.find(array, function (element) { return element.id == id });

            if (_.isEmpty(element)) {
                throw new Error("element from array " + arrayName + " by id " + id + " not found")
            }

            return element;
        },

        doFilterForwarding: function(rules, filteredMode) {
            var filtered = {}, ruleMode, includeAll = "*";

            _.each(rules, function(rule, name) {
                ruleMode = rule.mode;

                if (_.isEmpty(ruleMode)) {
                    ruleMode = includeAll;
                }

                ruleMode = _.isArray(ruleMode) ? ruleMode : [ruleMode];

                if (_.include(ruleMode, filteredMode) || _.include(ruleMode, includeAll)) {
                    filtered[name] = rule;
                } else {
                    console.log("ignore fields forwarding [" + name + "]");
                }
            });

            return filtered;
        },

        parseDialogContent: function (html) {
            return jQuery.parseHTML('<div>' + html + '</div>')
        }
    });

    Previewer.new = function (context, settings, headerTempalate) {
        headerTempalate = Handlebars.compile(_.isEmpty(headerTempalate) ? HEADER_TEMPLATE_DEFAULT : headerTempalate)({
            asterisk: context
        });

        _.each(settings, function(setting) {

            _.each(setting.records, function(record) {
                // record.__view__ = Mustache.render(setting.template_result, {record: record, asterisk: setting.externalContext});
                
                record.__view__ = Handlebars.compile(setting.template_result)({
                    record: record,
                    asterisk: setting.externalContext
                });
            });

            _.each(setting.actions, function(action) {
                try {
                    action.__fa_icon__ = action["action-args"].view_type == 'list' ? 'fa-align-justify' : 'fa-file-o'
                } catch (error) {}
            });

            setting.__record_actions__ = _.filter(setting.actions, function(action) {
                return _.include(action.visible, "result");
            });

            setting.__impersonal_actions__ = _.filter(setting.actions, function(action) {
                return _.include(action.visible, "noresult");
            });
        });

        return new this(context, settings, headerTempalate).open();
    }

    return Previewer;
});
