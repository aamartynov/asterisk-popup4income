odoo.define('asterisk_popup4income.opencaller', function (require) {
    "use strict";

    var dialogTpl = 
        "<div class=\"row\">" +
            "<div class=\"col-xs-12\">" +
                "<div class=\"form-group\">" +
                    "<label>Number:</label>" +
                    "<input type=\"text\" class=\"form-control\" name=\"number\" value=\"+33449234454\">" +
                "</div>" +
            "</div>" +
        "</div>";

    var session = require('web.session');
    var SystrayMenu = require('web.SystrayMenu');
    var Widget = require('web.Widget');
    var PreviewerWidget = require('asterisk_popup4income.previewer');
    var Dialog = require('web.Dialog');

    var openCaller = Widget.extend({
        template: 'asterisk_popup4income.OpenCaller',

        events: {
            'click a': 'open'
        },

        open: function (event) {
            var self = this;
            var dialog = new Dialog(null, {
                title: 'Incoming call',
                size: 'small',
                $content: jQuery.parseHTML(dialogTpl),
                buttons: [{
                    text: "Do it!",
                    close: true,
                    click: function() {
                        var number = this.$el.find('input[name=number]').val();
                        self.fire(number);
                    }
                }]
            });

            dialog.open();
            event.stopPropagation();
        },

        fire: function (number) {
            return session.rpc('/asterisk_popup4income/fire', {number: number});
        }
    });

    SystrayMenu.Items.push(openCaller);
});
