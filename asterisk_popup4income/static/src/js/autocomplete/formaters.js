// NOTE - rename this module to executres
// and put in this place eval function aka `new Function () ...`

odoo.define('asterisk_popup4income.formaters', function (require) {

    function domainFormater(domain, context) {
        var functionSignature = _(context).keys().join(', ');
        var functionArguments = _.values(context);

        _.each(domain, function(predicat) {
            // predicat common kind - ['leftOperand', 'operator', '{placeholder.property}']
            if (_.isArray(predicat) && _.size(predicat) == 3) {
                // eval just 3-th element - '{placeholder.property}'
                try {
                    predicat[2] = (new Function(
                        functionSignature,
                        "return " + predicat[2]
                    )).apply(window, functionArguments);
                } catch (error) {
                    console.error("error when evaluation: ", error);
                }
            }
        });

        return domain;

    }

    return {
        domain: domainFormater
    }
});
