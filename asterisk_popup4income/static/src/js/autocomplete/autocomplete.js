odoo.define('asterisk_popup4income.autocomplete', function (require) {
    "use strict";
    
    var FormView = require("web.FormView");
    var ListView = require("web.ListView");
    var ListRecord = require('web.list_common').Record;
    var DataModel = require('web.DataModel');

    var formaters = require('asterisk_popup4income.formaters');

    var AUTOCOMPLETE_DELAY_MS = 600;

    function interpolationStrategy(rules, context) {
        return $.when(window.format(rules.write, context));
    }

    function baseFieldValueComputeStrategy(rules, context) {
        var promise;

        console.log("eval with context", context);

        if (rules.search) {
            var searchDomain, SearchModel;

            console.log("do search for autocomplete with rules", rules);
            
            SearchModel = new DataModel(rules.search.model);
            searchDomain = formaters.domain(rules.search.domain, context);

            promise = SearchModel.query()
               .filter(searchDomain)
               .limit(1)
               .all()
               .then(function (records) {
                   return _.head(records) || {};
               });
        } else {
            promise = $.when({});
        }

        return promise.then(function(searchRecord) {
            console.log("total context for autocomplete", {
                record: context.record,
                asterisk: context.asterisk,
                searchRecord: searchRecord
            });

            var functionSignature = "record, asterisk, search";
            var functionBody = "return " + rules.write;

            console.log("autocomplete function: " + "(" + functionSignature + ") => { " + functionBody + " }");

            try {
                return (new Function(
                    functionSignature,
                    functionBody
                ))(context.record, context.asterisk, searchRecord);
            } catch(error) {
                console.log("error while execute evaluation for autocomplete", error);
            }
        });
    }

    function computeFildsValues(autocomplete) {
        var allValuesFutures = [], writeValues = {};

        console.log("will use context for autocomplete: ", autocomplete);

        _.keys(autocomplete.forwarding).forEach(function(dstKey) {
            var rules = autocomplete.forwarding[dstKey], futureWriteValue;

            switch (rules.type) {
                case "base":
                    futureWriteValue = baseFieldValueComputeStrategy(rules, autocomplete);
                    break;
                case "interpolation": 
                    futureWriteValue = interpolationStrategy(rules, autocomplete);
                    break;
                default:
                    console.log("unknown strategy type [" + rules.type + "] for forwarding, pass field [" + dstKey + "]");
                    futureWriteValue = $.when();
            }

            futureWriteValue.then(function(value) {
                console.log("resolve value for [" + dstKey + "]", value);
                writeValues[dstKey] = value;
            });

            allValuesFutures.push(futureWriteValue);
        });

        return $.when.apply($, allValuesFutures).then(function() {
            return writeValues;
        });
    }

    // is the same form
    FormView.include({
        // override
        // imitation on_defaults_load
        load_defaults: function() {
            var self = this;

            return self._super.apply(self, arguments).then(function(record) {
                self.once('view_content_has_changed', self, function() {
                    setTimeout(function() {
                        self.trigger('load_defaults');
                    }, AUTOCOMPLETE_DELAY_MS);
                });
                return record;
            });
        },

        // override
        start: function() {
            var self = this;

            self.__ok_autocomplete_for_events__ = [];

            if (self.options.action.target === 'new') {
                $.when().done(function() {
                    self.if_need_go_to_edit_mode();
                });
            } else {
                self.once('attached', self, function (event) {
                    self.if_need_go_to_edit_mode();
                });
            }

            self.once('load_record', self, function (record) {
                setTimeout(function() {
                    self.trigger('delay_load_record', record);
                }, AUTOCOMPLETE_DELAY_MS);
            });
            
            // NOTE - `load_defaults` and `load_record` join with `_.each`
            self.once('load_defaults', self, function(record) {
                var autocomplete = this.get_autocomplete();

                if (autocomplete && !_.includes(self.__ok_autocomplete_for_events__, "load_defaults")) {
                     self.__ok_autocomplete_for_events__.push("load_defaults");
                     self.asterisk_popup4income_autocomplete(autocomplete);
                }
            });

            self.once('delay_load_record', self, function(record) {
                var autocomplete = this.get_autocomplete();

                if (autocomplete && !_.includes(self.__ok_autocomplete_for_events__, "delay_load_record")) {
                    self.__ok_autocomplete_for_events__.push("delay_load_record");
                    self.asterisk_popup4income_autocomplete(autocomplete);
                }

                // self.once('view_content_has_changed', self, function () {});
            });

            return self._super();
        },

        get_autocomplete: function () {
            if (this.options && this.options.action.context && this.options.action.context.popup4income_autocomplete) {
                return this.options.action.context.popup4income_autocomplete;
            }
        },

        if_need_go_to_edit_mode: function () {
            if (this.get_autocomplete()) {
                this.to_edit_mode();
            }      
        },

        asterisk_popup4income_autocomplete: function (autocompleteRules) {
            var autocompletePromise = computeFildsValues(autocompleteRules),
                self = this;
            
            return autocompletePromise.then(function(autocompleteOutput) {
                _.each(autocompleteOutput, function (value, key) {
                    var field = this.fields[key];

                    // NOTE - maybe `when().done` no need?
                    $.when().done(function() {
                        try {
                            field.set_value(value);
                        } catch(error) {
                            console.error(error);
                        }
                    });
                }, self);
            });
        }
    });

    ListView.include({

        forcedShow: function() {
            // if we no have recods on list
            self.$('table:first').show();
            self.$('.oe_view_nocontent').remove();
        },

        do_add_record_from_autcomplete: function (autocompleteRules) {

            if (this.editable()) {
                var autocompletePromise = computeFildsValues(autocompleteRules),
                    self = this;

                // NOTE - do autocomplete and after load defaults like form above!
                // this.editor.form.once("load_defaults", this, function() {
                   // $.when().then(function() {});
                // });

                autocompletePromise.then(function(autocompleteOutput) {
                    
                    self.forcedShow();
                    self.start_edition();
                    self.editor.form.once('load_record', self, function() {

                        _.each(autocompleteOutput, function(value, key) {
                            var field = this.editor.form.fields[key];

                            $.when().done(function() {
                                try {
                                    console.log("key [" + key + "]", value)
                                    field.set_value(value);
                                } catch(error) {
                                    console.error(error);
                                }
                            });

                        }, self);
                    });
                });
            }
        }
    });

    ListView.Groups.include({
        render: function () {
            var self = this;
            return this._super.apply(this, arguments).done(function(response) {
                if (self.view.options.action.context && self.view.options.action.context.popup4income_autocomplete) {
                    setTimeout(function() {
                        self.view.do_add_record_from_autcomplete(self.view.options.action.context.popup4income_autocomplete);
                    }, AUTOCOMPLETE_DELAY_MS);
                }
            });
        }
    });
});
