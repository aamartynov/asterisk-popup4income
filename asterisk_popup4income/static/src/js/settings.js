odoo.define('asterisk_popup4income.settings', function (require) {
    "use strict";

    var Previewer = require('asterisk_popup4income.previewer');
    var bus = require('bus.bus').bus;
    var QWeb = require("web.QWeb");
    var session = require('web.session');

    Handlebars.registerHelper('timestamp', function(timestamp, format) {
        if (!timestamp) {
            return null;
        }

        if (!_.isString(format)) {
            format = 'DD.MM.YYYY';
        }
        return moment(parseInt(timestamp) * 1000).format(format);
    });

    function prepareSettings(settings, externalContext) {
        return settings.map(function (setting) {
            var headRecord = null,
                isMatch = false,
                fieldsForwarding = false,
                actions = [];

            try {
                actions = JSON.parse(setting.actions);                
                fieldsForwarding = JSON.parse(setting.fields_forwarding);                
            } catch (error) {
                console.error(error);
            }

            if (setting.records && setting.records.length > 0) {
                headRecord = setting.records[0];
            }

            return {
                id: setting.id,
                domain: setting.domain,
                model: setting.model,
                model_display_field: setting.model_display_field,
                call_date: setting.call_date,
                name: setting.name,
                template_result: setting.template_result,
                tpl_result_name: setting.tpl_result_name,
                tpl_noresult_name: setting.tpl_noresult_name,
                records: setting.records,
                headRecord: headRecord,
                fieldsForwarding: fieldsForwarding,
                actions: actions,
                isMatch: !_.isEmpty(headRecord),
                externalContext: externalContext
            };
        });
    }

    function newPreviwer(message) {
        try {
            return Previewer.new(
                message.call,
                prepareSettings(message.settings, message.call),
                message.header
            ); 
        } catch(error) {
            console.log(error);
        }
    }

    $.when(
        session.rpc('/asterisk_popup4income/user_phone', {}),
        session.rpc('/asterisk_popup4income/open_ami_connection', {})
    ).done(function (userPhoneInfo, amiConnection) {

        var devChanel = "asterisk_popup4income.development." + userPhoneInfo.internal_number;
        var dialBeginChanel = amiConnection.id + ".Dial." + userPhoneInfo.internal_number;
        var dialEndChanel = "Dial.End";
        
        if (amiConnection.from_cache) {
            console.log('asterisk_popup4income: take AMI connection from cache');
        } else {
            console.log('asterisk_popup4income: open new AMI connection');
        }
        
        bus.on("notification", this, function(notifications) {
            notifications.forEach(function(notification) {
                var chanel = notification[0],
                    message = notification[1];

                console.log("recive message from chanel " + chanel + ":", message);

                _.defaults(message, {
                    call: {
                        caller_number: "[unknown]",
                        dial_number: "[unknown]",
                        call_date: "[unknown]"
                    },
                    settings: []
                });

                if (chanel === devChanel) {
                    newPreviwer(message);
                }

                if (chanel === dialBeginChanel) {
                    newPreviwer(message);
                }
            });
        });

        [devChanel, dialBeginChanel, dialEndChanel].forEach(function(chanel) {
            bus.add_channel(chanel);
            console.log("asterisk_popup4income: subscribe for chanel: " + chanel);
        });

        bus.start_polling();
    });
});
