# -*- coding: utf-8 -*-

from odoo import http
import logging

from .telephony import Ami
from .telephony import Manager


logger = logging.getLogger(__name__)

# NOTE:  auth='public' in all action! fix it!

class EyekraftAsteriskClick2dialCallInfoController(http.Controller):
    @http.route('/asterisk_popup4income/user_phone', type='json', auth='public')
    def user_phone(self, **kwargs):
        return http.request.env.user.phone_info()

    @http.route('/asterisk_popup4income/open_ami_connection', type='json', auth='public')
    def open_ami_connection(self, **kwargs):
        server = http.request.env.user.get_asterisk_server_from_user()
        from_cache = Manager.open(server.id, http.request.env.cr.dbname, server.ip_address,
                                  server.port, server.login, server.password)

        return {'from_cache': from_cache, 'id': server.id}

    @http.route('/asterisk_popup4income/fire', type='json', auth='public')
    def fire(self, **kwargs):
        user_phone_info = http.request.env.user.phone_info()
        caller_number = http.request.params['number']
        dial_number = user_phone_info["internal_number"]

        output_settings = http.request.env["popup4income.settings"].search_records(
            dial_number,
            caller_number,
            http.request.env.user.get_asterisk_server_from_user().id
        )

        http.request.env['bus.bus'].sendone(
            "asterisk_popup4income.development.%s" % user_phone_info["internal_number"], output_settings)
        
        logger.info("Fake incoming call for %s" % caller_number)
