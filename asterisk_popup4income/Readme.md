# Документация к модулю odoo asterisk_popup4income

Краткое описание полей для настойки поиска по базе odoo:
- `title` - общий заголовок для найденных записей
- `model` - модель, по для которой будет производится поиск
- `domain` - домен поиска, стандартный синтаксис домена в odoo, с одним отличием - если пропущен второй операнд, на его место автоматически подставляется номер звонящего - `[('mobile_phone', '=')]` 
- `template_for_result` - шаблон для отображения найденной модели, в виде HTML
- `fields_forwarding` - настройка "проброса" в виде JSON значений из котекстов поиска в запись/записи после выполнения действия (нажатия на кнопку)
- `actions` - настройка действий допустимых для записей данной модели в виде JSON.


# Примеры

`title`: `Сотрудники`
`model`: `hr.employee`
`domain`: `[('mobile_phone', '=')]`
`template_for_result`: 
```html
<div>
    <i>{{record.name}}</i>
</div>
```
`fields_forwarding`:
```json
{
    "hr.employee": {
          "mobile_phone": {"type": "interpolation", "write": "{asterisk.caller_number}"},
          "work_phone": {"type": "interpolation", "write": "{asterisk.dial_number}"},
          "notes": {"type": "interpolation", "write": "Был набран - {asterisk.dial_number}"}
     },
    "res.partner": {
          "mobile": {"type": "interpolation", "write": "{asterisk.caller_number}"}
     }
}
```
`actions`:
```json
[
    {
        "action": "204",
        "action-args": {
            "view_type": "form"
        },
        "title": "Создать",
        "visible": ["noresult"]
    },
    {
        "action": "204",
        "action-args": {
            "view_type": "form",
            "res_id": "."
        },
        "title": "Открыть",
        "visible": ["result"]
    }
]
```

