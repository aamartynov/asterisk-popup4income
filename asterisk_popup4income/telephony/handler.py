
import logging
import phonenumbers
import re

from .. import tool


logger = logging.getLogger(__name__)


class BaseHandler(object):
    _environment = None

    @classmethod
    def factory(cls, environment):
        self = cls()
        self._environment = environment

        return self

    def __call__(self, event):
        try:
            self.__recive__(event)
        except Exception:
            logger.exception('Error while processing event')

    def __recive__(self, event):
        raise NotImplementedError("you must implement __recive__ method")

    @property
    def environment(self):
        return self._environment

    def _send(self, chanel, message):
        logger.info("send message to chanel: %s" % chanel)

        with tool.model(self._environment.dbname, 'bus.bus') as bus:
            bus.sendone(chanel, message)



class DialEndHandler(BaseHandler):
    def __recive__(self, event):
        self._send("Dial.End", event.properties)


class DialBeginHandler(BaseHandler):
    
    def __init__(self):
        self._formatter = PhoneReaderChain([PhoneCleaner(), IfNeedFormat(PhoneFormatter())])

    def __recive__(self, event):

        # replace with filter from `ami` packet
        if 'CallerIDNum' not in event or 'CallerIDName' not in event or 'Dialstring' not in event:
            return

        try:
            caller_number = self._try_read_phone_from(event, ['CallerIDNum', 'CallerIDName'])
            dial_number = self._try_read_phone_from(event, ['Dialstring'])
        except ErrorPhoneParse:
            return

        logger.info("detected incoming call %s => %s" % (caller_number, dial_number))
        
        # NOTE - check if user by `dial_number` was not found
        # and not fire long polling message to browser

        search = self._search(dial_number, caller_number, event)
        self._send("%s.Dial.%s" % (self.environment.server_id, dial_number), search)

    def _try_read_phone_from(self, event, keys):
        if not len(keys):
            raise ValueError("keys must be non empty list")

        for key in keys:
            logger.info(u'try to read number from property [%s]' % key)
            try:
                return self._formatter(event[key])
            except (ErrorPhoneParse, KeyError) as error:
                logger.exception(error)

        raise ErrorPhoneParse

    def _search(self, dial_number, caller_number, event):
        with tool.model(self.environment.dbname, "popup4income.settings") as model:
            return model.search_records(dial_number, caller_number, self.environment.server_id, event)


class IngnoreEventException(Exception): pass
class ErrorPhoneParse(Exception): pass


class PhoneReaderChain(object):
    _readers = None

    def __init__(self, readers):
        self._readers = readers

    def read(self, value):
        for reader in self._readers:
            value = reader(value)

        return value

    def __call__(self, value):
        return self.read(value)


class BasePhoneReader(object):
    def __call__(self, value):
        raise NotImplementedError


class PhoneCleaner(BasePhoneReader):
    # NOTE: remove member _chars
    _chars = u'/', u'-', u'@'

    _pattern = u'(\d+)'

    def __call__(self, value_before):
        if not value_before:
            raise ErrorPhoneParse(u'number is empty')

        search = re.search(self._pattern, value_before)

        if not search:
            raise ErrorPhoneParse(u'number is empty')

        value_after = search.group(0)

        logger.info(u'clean number %s => %s' % (value_before, value_after))

        return value_after


class PhoneFormatter(BasePhoneReader):
    _default_region = "RU"

    def __call__(self, value_before):
        try:
            value_after = phonenumbers.format_number(
                phonenumbers.parse(value_before, self._default_region),
                phonenumbers.PhoneNumberFormat.E164
            )
        except phonenumbers.NumberParseException as error:
            raise ErrorPhoneParse(error)
        
        logger.info(u'format number %s => %s' % (value_before, value_after))
        return value_after


class IfNeedFormat(BasePhoneReader):
    
    def __init__(self, formatter):
        self._formatter = formatter

    def __call__(self, value):
        if len(value) > 4:
            logger.info(u'number %s need fromat' % value)
            return self._formatter(value)
        
        logger.info(u'number %s not need fromat' % value)

        return value
