"""
odoo specify wrapper for ami packet
"""

import logging
import threading
import ami

from .handler import DialBeginHandler


logger = logging.getLogger(__name__)


handlers = [
    ({'Event': 'Dial'}, DialBeginHandler)
]

class Manager(object):
    _cache = {}
    
    @classmethod
    def open(cls, server_id, dbname, host, port, username, password):
        """
        return True if take connection from cahe else return False
        """

        if server_id not in cls._cache.keys():
            
            def open():
                try:
                    return cls._open(server_id, dbname, host, port, username, password)
                except Exception:
                    logger.exception('Error when serve AMI connection')

            thread = cls._cache[server_id] = threading.Thread(target=open)
            thread.daemon = True
            thread.start()

            from_cache = False
        else:
            from_cache = True

        return from_cache

    @classmethod
    def _open(cls, server_id, dbname, host, port, username, password):
        manager = ami.Manager(host, port, username, password)
        environment = Environment(server_id, dbname)

        # register events
        for predicat, handler_class in handlers:
            manager.event.when(predicat)(handler_class.factory(environment))
        
        # open and listen asterisk socket
        # this wil block current thread
        manager.serve_forever()

        # remove from cache
        del cls._cache[server_id]

        logger.info('Exit from serve AMI - id={id}, host={host}'.format(
            id=server_id,
            host=host
        ))


class Environment(object):
    def __init__(self, server_id, dbname):
        self._server_id = server_id
        self._dbname = dbname

    @property
    def dbname(self):
        return self._dbname

    @property
    def server_id(self):
        return self._server_id
