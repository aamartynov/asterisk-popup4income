# -*- coding: utf-8 -*-

{
    'name': 'Pop-up income calls widget',
    'version': '10.0.12.03.2018',
    'category': 'Phone',
    'license': 'AGPL-3',
    'description': '''Smart and simple pop-up income calls widget by use Asterisk AMI.
                      Compatible with any Asterisk servers like FreePBX, Issabel, AsteriskNow, Elastix, TrixBox.''',
    'author': "Martynov Alexander. Eyekraft company",
    'depends': ['asterisk_click2dial'],
    'qweb': ['static/src/xml/*.xml'],
    'data': [
        'asterisk_popup4income.xml',
        'views/settings.xml',
        'security/ir.model.access.csv'
    ],
    'demo': ['demo/asterisk_popup4income_demo.xml'],
    'installable': True,
    'application': True
}
