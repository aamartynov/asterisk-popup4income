import datetime
import logging
import pytz
from odoo.modules.registry import RegistryManager
from odoo import api, SUPERUSER_ID
import odoo
from contextlib import contextmanager
import threading

logger = logging.getLogger(__name__)


def time(timezone=None):
    if timezone:
        date = datetime.datetime.now(pytz.timezone(timezone))
    else:
        date = datetime.datetime.now()
        logger.info('Take time with default timezone')
        
    return date.strftime('%d.%m.%Y %H:%M:%S')


def select_db_name():

    if hasattr(select_db_name, 'db_name'):
        return select_db_name.db_name

    db_names = odoo.service.db.list_dbs(True)
    
    if len(db_names) > 1:
        raise RuntimeError('multiple database found')

    for db_name in db_names:
        select_db_name.db_name = db_name
        return select_db_name()

    raise RuntimeError('database not found')


@contextmanager
def db(name):
    with api.Environment.manage():
        registry = RegistryManager.get(name)
        with registry.cursor() as cursor:
            yield api.Environment(cursor, SUPERUSER_ID, {})


@contextmanager
def model(db_name, model_name):
    with db(db_name) as environment:
        yield environment[model_name]


def inject(wrapped):
    """
    Add to positional arguments as
    last item odoo.api.Environment instance.
    """

    def wrapper(*args, **kwargs):
        with api.Environment.manage():
            registry = RegistryManager.get(select_db_name())
            with registry.cursor() as cursor:
                environment = api.Environment(cursor, SUPERUSER_ID, {})
                return wrapped(* (args + (environment,)), **kwargs)

    return wrapper


def wait_for_registry(db_name):
    """
    Wait for call wrapped function until
    rigystry[db_name] will ready state
    """

    def wrapper(wrapped):
        
        @inject
        def waiter(environment):
            """ TODO - maybe need return future from waiter? """

            def watcher():
                registry = RegistryManager.get(db_name)
                logger.info("try for registry[%s]" % db_name)

                if not registry.ready:
                    threading.Timer(5, watcher).start()
                else:
                    logger.info("registry[%s] ready" % db_name)
                    wrapped(environment)

            watcher()

        return waiter

    return wrapper
