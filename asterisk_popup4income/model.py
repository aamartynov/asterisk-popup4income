# -*- coding: utf-8 -*-

from odoo import models, api, fields
from odoo.addons.base_phone import common
import time
import logging
import ami

from . import tool


logger = logging.getLogger(__name__)



# monkey patching
__convert_all_phone_fields__ = common.convert_all_phone_fields

def convert_all_phone_fields(self, vals, fields_to_convert):
    company = self.env.user.company_id

    if not hasattr(company, 'use_convert_phone_field') or company.use_convert_phone_field:
        return __convert_all_phone_fields__(self, vals, fields_to_convert)
    else:
        return vals.copy()


common.convert_all_phone_fields = convert_all_phone_fields


class RecordsetJsonConverter(object):
    _serialized_type_values = str, unicode, bytes, bool, int, float
    _unserialized_type_fields = fields.Many2one, fields.One2many, fields.Many2many
    _date_type_fields = fields.Datetime, fields.Date

    def _serialize_date(self, date_type, value):
        return value

    def _serialize_recordset(self, recordset_relation, recordset):
        output_records = []
        serialized_fileds = [_ for _ in recordset._fields.keys() if _ in (
            'id',
            'name',
            'display_name'
        )]

        for record in recordset:
            serialized_record = {}

            for field_name in serialized_fileds:
                field_value = record[field_name]
                
                if isinstance(field_value, self._serialized_type_values):
                    serialized_record[field_name] = field_value

            output_records += serialized_record,

        if recordset_relation is fields.Many2one:
            return next(iter(output_records), None) or {}

        return output_records

    # NOTE - maybe do it staticaly
    def to_json_support(self, recordset):
        output_records = []

        for record in recordset:
            templatify_record = {}

            for field_name in record._fields.keys():
                field_value = record[field_name]
                FieldType = type(recordset._fields[field_name])

                if field_value and FieldType in self._date_type_fields:
                    field_value = time.mktime(FieldType.from_string(field_value).timetuple())
                elif FieldType in self._unserialized_type_fields:
                    field_value = self._serialize_recordset(FieldType, field_value)
                elif not isinstance(field_value, self._serialized_type_values):
                    field_value = None
                    logger.error('type %s field not siralized' % type(field_value).__name__)

                templatify_record[field_name] = field_value

            output_records += templatify_record,

        return output_records


class ResCompany(models.Model):
    _inherit = 'res.company'

    popup4income_header_template = fields.Text(
        string='Popup header template'
    )
    
    use_convert_phone_field = fields.Boolean(
        string='Use convert phone value by country code',
        default=True)


class BaseConfigSettings(models.TransientModel):
    _inherit = 'base.config.settings'

    popup4income_header_template = fields.Text(
        string='Popup header template',
        related="company_id.popup4income_header_template",
        default='<i class="fa fa-phone-square" aria-hidden="true"></i>{{asterisk.caller_number}}<br/>'
                '<small>'
                '<i class="fa fa-clock-o" aria-hidden="true"></i>'
                '{{asterisk.date}}, {{asterisk.event.CallerIDName}}'
                '</small>'
    )

    use_convert_phone_field = fields.Boolean(
        related='company_id.use_convert_phone_field')


class Base(models.AbstractModel):
    _inherit = 'base'

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, values):
        return super(Base, self).create(values)


class ResUsers(models.Model):
    _inherit = "res.users"

    popup4income_settings_group = fields.Many2one('popup4income.settings.group', string='Rules group')
    
    @api.multi
    def phone_info(self):
        return dict(
            internal_number=self.internal_number,
            dial_suffix=self.dial_suffix,
            callerid=self.callerid
        )

class SettingsGroup(models.Model):
    _name = 'popup4income.settings.group'

    name = fields.Char(string='Name')
    description = fields.Text(string='Description')
    settings = fields.Many2many('popup4income.settings', string='AsteriskPopup4incomeSettings')
    color = fields.Integer(string='Color')


class Settings(models.Model, RecordsetJsonConverter):
    _name = "popup4income.settings"
    _description = "Settings for asterisk_popup4income income calls"
    _order = "sequence"

    name = fields.Char(string='Name')
    sequence = fields.Integer(string='Sequence')
    terminal = fields.Boolean(string='Terminal')
    template_result = fields.Text(string='Result template')
    model = fields.Char(string='Model')
    domain = fields.Char(string='Domain')
    fields_forwarding = fields.Text(string='Fields forwarding')
    actions = fields.Text(string='JSON actions definition')
    groups = fields.Many2many('popup4income.settings.group', string='AsteriskPopup4incomeSettingsGroups')
    color = fields.Integer(string='Color')

    def search_records(self, dial_number, caller_number, asterid, ami_event=None):
        user, settings = self._do_search_records(dial_number, caller_number, asterid)
        return {
            "header": user.company_id.popup4income_header_template,
            "call": {
                "caller_number": caller_number,
                "dial_number": dial_number,
                "date": tool.time(user.tz),
                "event": ami_event or {}
            },
            "settings": settings
        }

    @api.multi
    def _do_search_records(self, search_user_phone, search_domain_value, asterid):
        """
        :search_domain_value - value that will be use for search records
        """
        
        output_settings = []
        output_user = self._search_user_by_phone_number(search_user_phone, asterid)
        settings = self._search_settings_by_user(output_user)

        for setting in settings:
            try:
                parsed_domain = self._parse_domain(setting.domain, search_domain_value)
            except Exception as error:
                logger.exception(error)
            else:
                records = None
                try:
                    records = self.env[setting.model].search(parsed_domain)
                except KeyError as error:
                    logger.error(str(error))
  
                output_settings += {
                    "id": setting.id,
                    "domain": setting.domain,
                    "model": setting.model,
                    "name": setting.name,
                    "template_result": setting.template_result,
                    "fields_forwarding": setting.fields_forwarding,
                    "actions": setting.actions,
                    # NOTE - is old way serialized recordset, just was used search_read
                    # records = self.env[setting.model].search_read(parsed_domain)
                    "records": self.to_json_support(records) if records is not None else []
                },

                if setting.terminal and len(records):
                    break

        return output_user, output_settings

    def _search_user_by_phone_number(self, dial_number, asterid):
        return self.env["res.users"].search([
            '&', '|', '|',
            ("asterisk_server_id", "=", asterid),
            ("resource", "=", dial_number),
            ("internal_number", "=", dial_number),
            ("resource", "=", dial_number)
        ], limit=1)

    @api.multi
    def _search_settings_by_user(self, user):
        if user and user.popup4income_settings_group and user.popup4income_settings_group.settings:
            return user.popup4income_settings_group.settings
        else:
            return []

    def _parse_domain(self, domain_value, search_value):
        """
            NOTE - reimplement this with text parsing without eval.
            NOTE - also rewirte with string interpolation use:
                str.format(asterisk=AseriskContext())
            where:
                class AseriskContext:
                    caller_number = None
                    dial_number = None
        """
        
        try:
            domain = eval(domain_value, {'__builtins__': {}})
        except Exception as error:
            raise ParseError(error)
        else:
            if not isinstance(domain, (list, tuple)):
                raise ParseError("domain must be tuple or list")
        
        def domain_element_mapper(domain_item):
            
            if isinstance(domain_item, str):
                return domain_item
            
            if isinstance(domain_item, tuple):
                if len(domain_item) == 2:
                    return domain_item + (search_value,)
                elif len(domain_item) == 3:
                    return domain_item

            raise ParseError('domain item must be tuple 2 or 3 items number '
                             'or string (not %s)' % domain_item.__class__.__name__)

        return map(domain_element_mapper, domain)


class ParseError(Exception): pass
